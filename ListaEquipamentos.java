public class ListaEquipamentos {
	public static void main(String [] arg){
		try{
			Produto [] equipamentos = new Equipamento[]{
				new Equipamento(1, "Antena"),
				new Equipamento(2, "Roteador"),
				new Equipamento(3, "ONU"),
				new Equipamento(4, "RB"),
				new Equipamento(5, "OLT")
			};
			for(Equipamento item: equipamentos){
				System.out.println(item.toString());
			}
		}catch(Exception ex){
			System.out.println(ex);
		}
	}
}
