public class Equipamento {
	//Declarar atributos (propriedades)
	int codigo;
	//Ex: nome="Roteador";
	String nome;
	//Construtor
	//altera atributos do objeto
	public Equipamento(int cod, String nome)throws Exception{
		setCodigo(cod);
		setNome(nome);
	}
	//Métodos
	// retorna valor (boolean)
	// estático
	public static boolean validarNome(String novo){
		boolean resultado=false;
		if(novo.length() > 2){
			resultado = true;
		}else if(novo.toUpperCase()==novo.toString()){
			//SE("ONU"=="onu");
			if(novo.length() > 1){
				if(novo.length() > 1){
					resultado = true;
				}
			}
		}
		return resultado;
	}
	//Métodos set e get
	public String getNome(){
		return nome;
	}
	public int getCodigo(){
		return codigo;
	}
	//Método protect
	//lança exceção
	protected void setNome(String novo)throws Exception{
		if(validarNome(novo)==true){
			this.nome = novo;
		}else {
			throw new NomeInvalidoException();
		}
	}
	// private
	private void setCodigo(int cod){
		this.codigo = cod;
	}
	public String toString(){
		return getCodigo() + "; " + getNome();
	}
}
//exceção criada
class NomeInvalidoException extends Exception {
	public String toString(){
		return "Nome inválido!";
	}
}
